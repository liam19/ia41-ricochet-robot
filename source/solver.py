import board_config as bc
import math
import queue
from state import State
from time import sleep


class Solver:

    direction_offset = {"N": (-1, 0), "E": (0, 1), "S": (1, 0), "W": (0, -1)}

    def __init__(self, board):
        """Constructeur de la classe Solver."""
        self.board = board

    def _heuristic_manhattan_d(self, node, end_node):
        """
            Méthode retournant la distance de Manhattan entre deux noeuds node et end_node.
        """
        return abs(end_node[1] - node[1]) + abs(end_node[0] - node[0])

    def _heuristic_crow_flies(self, node, end_node):
        """
            Méthode retournant la distance distance à vol d'oiseau entre deux noeuds node et end_node.
        """
        return math.sqrt(
            abs(end_node[1] - node[1]) ^ 2 + abs(end_node[0] - node[0]) ^ 2
        )

    def _get_index_of_goal_robot(self, goal):
        """
            Méthode permettant de déterminer la couleur du robot objectif, 
            à partir de la couleur de l'objectif.

            Paramètre
            ---------
            goal : tuple contenant les coordonnées de l'objectif.

            Retour
            ------
            Entier représentant l'index de la couleur correspondant au robot objectif.
        """
        try:
            goal_color = set(self.board[goal]).intersection(
                set(bc.COLORS)).pop()
        except KeyError:
            raise KeyError("Objectif invalide.")

        return bc.COLORS.index(goal_color)

    def _successor_robot_positions(self, robot_positions):
        """
            Méthode permettant de déterminer tous les successeurs (dans toutes les directions) de tous les robots à un moment donné.

            Paramètre
            ---------
            robot_positions : tuple contenant les postions de tous les robots (sous forme de tuple).

            Retour
            ------ 
            Liste de tous les successeurs.
        """
        successors = []
        robot_positions_set = set(robot_positions)
        for i, robot_position in enumerate(robot_positions):
            for direction in bc.DIRECTIONS:
                successor_sq = self._destination_sq(
                    direction, robot_position, robot_positions_set
                )
                successor_positions = list(robot_positions)
                successor_positions[i] = successor_sq
                successors.append(tuple(successor_positions))

        return successors

        # Pourquoi un set pour robot_positions_set ?
        # Un set est un ensemble d'éléments uniques.
        # Cela nous permet d'être sûr de ne pas visiter plusieurs fois les mêmes emplacements.

    def _destination_sq(self, direction, starting_sq, robot_positions_set):
        """
            Méthode permettant de déterminer la position suivante d'un robot suivant une direction donnée.
            La boucle s'arrêtera lorsque le caractère indiquant la direction 
            sera contenu dans la chaîne représentant la position destination. 

            Paramètres
            ----------
            - direction: caractère (char), représentation de la direction (ex : 'N' pour NORTH).

            - starting_sq: tuple contenant la position de départ du robot.

            - robot_positions_set: set contenant toutes les positions explorées.

            Retour
            ------
            Tuple correspondant à la destination du robot après déplacement.
        """
        current_sq = starting_sq
        try:
            while direction not in self.board[current_sq]:
                next_sq = self._tuple_add(
                    current_sq, self.direction_offset[direction])
                # Si on voit que la position a déjà été explorée, on sort de la boucle
                if next_sq in robot_positions_set:
                    break
                current_sq = next_sq
        except IndexError:
            return current_sq
        return current_sq

    def _tuple_add(self, a, b):
        """
            Méthode permettant d'additionner les coordonnées de deux tuples.
        """
        return (a[0] + b[0], a[1] + b[1])

    def breadth_first_search(self, goal, starting_robot_positions):
        """
            Implémentation du BFS pour la résolution du Ricochet Robots.
        """
        closedStates = set()  # Contiendra toutes les positions déjà vues
        openStates = queue.Queue()  # Stockera les noeuds "ouverts"
        openStates.put(
            [starting_robot_positions]
        )  # On commence par explorer tous les noeuds de départ
        goal_robot_position_index = self._get_index_of_goal_robot(goal)

        # Tant que la queue (liste ouverte) n'est pas vide...
        while openStates:
            current_path = openStates.get()  # On récupère la liste en tête
            # On récupère la position en fin de liste
            final_position = current_path[-1]
            if final_position[goal_robot_position_index] == goal:
                return current_path
            for successor in self._successor_robot_positions(final_position):
                if successor not in closedStates:
                    closedStates.add(successor)
                    openStates.put(current_path + [successor])
        return "Aucun chemin trouvé !"

    def a_star(self, goal, starting_robot_positions):
        """
            Implémentation du A* pour la résolution du Ricochet Robots ==> NON FONCTIONNEL 
        """
        openList = []
        closedList = []

        starting_state = State(starting_robot_positions, 0)
        starting_state.print_state()

        openList.append(starting_state)

        r_goal_index = self._get_index_of_goal_robot(goal)

        while openList:
            # Récupération de l'état avec la plus petite heuristique (dans openList)
            actual_state = openList.pop(openList.index(
                min(openList, key=lambda x: x.heuristic)))

            closedList.append(actual_state)

            if actual_state.r_positions[r_goal_index] == goal:
                print('Chemin trouvé !')
                return actual_state.reconstruct_path()

            # print(actual_state.r_positions[r_goal_index],'!=', goal)

            # Si cet état n'est pas dans closedList et que cet état n'est pas dans openList avec une plus petite valeur d'heuristique
            # if actual_state not in closedList and not actual_state.inf_heuristic_equal_positions(openList):
            # On parcourt les successeurs de notre état actuel et on calcule leurs heuristiques
            for successor_pos in self._successor_robot_positions(actual_state.r_positions):
                g_value = actual_state.heuristic + 1  # Distance depuis l'état initial

                # Calcul de la distance heuristique entre le robot cible et la destination
                h_value = self._heuristic_manhattan_d(
                    successor_pos[r_goal_index], goal)

                # Calcul de l'heuristique totale
                f_value = g_value + h_value

                # Création du nouvel état
                successor_state = State(successor_pos, f_value, actual_state)

                if successor_state not in set(openList).union(set(closedList)) or not successor_state.inf_heuristic_equal_positions(openList):
                    # Ajout du nouvel état à openList
                    openList.append(successor_state)

        print("Aucun chemin trouvé !")

    def show_solution(self, solution):
        print(
            "\nLes coordonnées suivantes représentent respectivement : Robot Rouge, Robot Jaune, Robot Vert et Robot Bleu."
        )
        print(
            "\nAttention : les coordonnées sont présentées de la manière suivante : (ligne, colonne).\n"
        )
        print(
            "{} mouvement{} nécessaires pour atteindre l'objectif :".format(
                len(solution) - 1, "" if len(solution) == 1 else "s"
            )
        )
        for i, step in enumerate(solution[1:], 1):
            if type(step) is State:
                print("Etape " + str(i) + " : ",
                      ", ".join((str(robot.r_positions) for robot in step)))
            else:
                print("Etape " + str(i) + " : ",
                      ", ".join((str(robot) for robot in step)))
