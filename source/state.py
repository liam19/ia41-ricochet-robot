class State:
    """
        Classe définissant un état de notre système.
        Un état est composé :
        - De l'ensemble des positions des robots (ensemble de tuples)
        - D'une valeur d'heuristique correspondant à cet état (entier)
    """

    def __init__(self, r_positions, heuristic=0, previous_state=None):
        self.r_positions = r_positions
        self.heuristic = heuristic
        self.previous_state = previous_state


    def inf_heuristic_equal_positions(self, tab_states):
        """
            Méthode permettant de déterminer si un état avec une position similaire 
            mais une plus petite heuristique est présent dans un tableau de State.
        """
        for state in tab_states:
            if state.r_positions == self.r_positions and state.heuristic < self.heuristic:
                return True
        return False

    def sup_heuristic_equal_positions(self, tab_states):
        for i, state in enumerate(tab_states):
            if state.r_positions == self.r_positions and state.heuristic > self.heuristic:
                return i
        return -1

    def reconstruct_path(self):
        """
            Permet de reconstruire le chemin à partir de la valeur de l'état précédent du noeud.
        """
        path = []
        state = self
        while state.previous_state != None:
            path.append(state)
            state = state.previous_state
        path.append(state)
        return path.__reversed__()


    def print_state(self):
        print('Etat actuel :', self.r_positions, ' | Heuristique de cet état :', self.heuristic)

