import numpy as np
import random

# Directions des murs
NORTH = "N"
EAST = "E"
SOUTH = "S"
WEST = "W"

# Tuple des directions possibles
DIRECTIONS = (NORTH, EAST, SOUTH, WEST)


# Couleurs - s'appliquent lors de la description des robots ET des objectifs
RED = "R"
YELLOW = "Y"
GREEN = "G"
BLUE = "B"

# Tuple des couleurs
COLORS = (RED, YELLOW, GREEN, BLUE)


# Formes des objectifs - s'associent avec les couleurs (Ex : GT signifie GREEN TRIANGLE)
CIRCLE = "C"
TRIANGLE = "T"
SQUARE = "Q"
HEXAGON = "H"

# Tuple des formes
SHAPES = (CIRCLE, TRIANGLE, SQUARE, HEXAGON)


# Dictionnaire permettant de transformer l'orientation des murs,
# en effectuant une rotation dans le sens des aiguilles du montre
rotated_wall_transformations = {NORTH: EAST, EAST: SOUTH, SOUTH: WEST, WEST: NORTH}


def make_quadrant(quadrant):
    """Fonction permettant de créer une matrice 8x8, à partir d'une liste 1 dimension."""
    return np.array(quadrant).reshape((8, 8))


# Tous les quadrants sont décris tels qu'ils correspondent au coin supérieur gauche du plateau.

# Il sera par la suite nécessaire d'effectuer le nombre de rotations adéquat
# avant de fusionner les quadrants pour former le plateau de jeu.

QUAD_1 = make_quadrant(
    [
        "NW",
        "N",
        "N",
        "NE",
        "NW",
        "N",
        "N",
        "N",
        "W",
        "X",
        "X",
        "X",
        "X",
        "SEGH",
        "W",
        "X",
        "WE",
        "SWRQ",
        "X",
        "X",
        "X",
        "N",
        "X",
        "X",
        "SW",
        "N",
        "X",
        "X",
        "X",
        "X",
        "S",
        "X",
        "NW",
        "X",
        "X",
        "X",
        "X",
        "E",
        "NWYC",
        "X",
        "W",
        "X",
        "S",
        "X",
        "X",
        "X",
        "X",
        "X",
        "W",
        "X",
        "NEBT",
        "W",
        "X",
        "X",
        "X",
        "S",
        "W",
        "X",
        "X",
        "X",
        "X",
        "X",
        "E",
        "NW",
    ]
)

QUAD_2 = make_quadrant(
    [
        "NW",
        "N",
        "N",
        "NE",
        "NW",
        "N",
        "N",
        "N",
        "W",
        "X",
        "X",
        "X",
        "X",
        "X",
        "X",
        "X",
        "W",
        "X",
        "X",
        "X",
        "X",
        "SEBH",
        "W",
        "X",
        "W",
        "X",
        "S",
        "X",
        "X",
        "N",
        "X",
        "X",
        "SW",
        "X",
        "NEGC",
        "W",
        "X",
        "X",
        "X",
        "X",
        "NW",
        "S",
        "X",
        "X",
        "X",
        "X",
        "E",
        "SWRT",
        "WE",
        "NWYQ",
        "X",
        "X",
        "X",
        "X",
        "X",
        "NS",
        "W",
        "X",
        "X",
        "X",
        "X",
        "X",
        "E",
        "NW",
    ]
)

QUAD_3 = make_quadrant(
    [
        "NW",
        "NE",
        "NW",
        "N",
        "NS",
        "N",
        "N",
        "N",
        "W",
        "S",
        "X",
        "E",
        "NWRC",
        "X",
        "X",
        "X",
        "W",
        "NEGT",
        "W",
        "X",
        "X",
        "X",
        "X",
        "X",
        "W",
        "X",
        "X",
        "X",
        "X",
        "X",
        "SEYH",
        "W",
        "W",
        "X",
        "X",
        "X",
        "X",
        "X",
        "N",
        "X",
        "SW",
        "X",
        "X",
        "X",
        "X",
        "X",
        "X",
        "X",
        "NW",
        "X",
        "E",
        "SWBQ",
        "X",
        "X",
        "X",
        "S",
        "W",
        "X",
        "X",
        "N",
        "X",
        "X",
        "E",
        "NW",
    ]
)

QUAD_4 = make_quadrant(
    [
        "NW",
        "N",
        "N",
        "N",
        "NE",
        "NW",
        "N",
        "N",
        "W",
        "X",
        "SERH",
        "W",
        "X",
        "X",
        "X",
        "X",
        "W",
        "X",
        "N",
        "X",
        "X",
        "X",
        "X",
        "X",
        "WE",
        "SWGQ",
        "X",
        "X",
        "X",
        "X",
        "S",
        "X",
        "SW",
        "N",
        "X",
        "X",
        "X",
        "E",
        "NWYT",
        "X",
        "NW",
        "X",
        "X",
        "X",
        "X",
        "S",
        "X",
        "X",
        "W",
        "X",
        "X",
        "X",
        "X",
        "NEBC",
        "W",
        "S",
        "W",
        "X",
        "X",
        "X",
        "X",
        "X",
        "E",
        "NW",
    ]
)


def generate_random_board():
    """
        Fonction permettant de créer et renvoyer un tableau de quadrants aléatoirement piochés dans BOARDS.

        Retour
        ------
        Liste de tous les quadrants réorganisés aléatoirement.
    """
    tab_rand_quads = random.sample(BOARDS, 4)
    return tab_rand_quads


def rotate_square_clockwise(sq_vals, num_rotations=1):
    """
        Fonction permettant d'effectuer la rotation d'une case, en changeant sa valeur,
        en fonction de sa valeur actuelle et du nombre num_rotations de rotations à effectuer.
    """
    new_sq_vals = sq_vals
    for _ in range(num_rotations):
        sq_vals = new_sq_vals
        new_sq_vals = ""
        for val in sq_vals:
            if val in rotated_wall_transformations:
                new_sq_vals += rotated_wall_transformations[val]
            else:
                new_sq_vals += val
    return new_sq_vals


# Permet de "vectorize" la fonction rotate_square_clockwise
# -- la fonction va s'appliquer à tous les éléments de np.rot90(quad, -num_rotations)
rotate_squares_clockwise = np.vectorize(rotate_square_clockwise)


def rotate_quad_clockwise(quad, num_rotations=1):
    """
        Fonction permettant d'effectuer num_rotations rotations du quadrant quad.
    """
    return rotate_squares_clockwise(np.rot90(quad, -num_rotations), num_rotations)


def combine_quads_into_single_board(top_left, top_right, bottom_left, bottom_right):
    """
        Fonction permettant de fusionner les quadrants en un seul plateau, en effectuant les rotations adéquates.
        
        Paramètres
        ----------
        top_left, top_right, bottom_left, bottom_right : quadrants du plateau (tableaux)

        Retour
        ------
        matrice correspondant au plateau de jeu complet.
    """
    top_left = rotate_quad_clockwise(top_left, 0)
    top_right = rotate_quad_clockwise(top_right, 1)
    bottom_right = rotate_quad_clockwise(bottom_right, 2)
    bottom_left = rotate_quad_clockwise(bottom_left, 3)

    return np.bmat([[top_left, top_right], [bottom_left, bottom_right]])


# Liste de tous les quadrants pouvant former le plateau de jeu final.
BOARDS = [QUAD_1, QUAD_2, QUAD_3, QUAD_4]


def random_robots_positions():
    """
        Fonction générant aléatoirement les positions des robots.

        Retour
        ------
        Un tuple de tuples contenant les positions.
    """
    return tuple([((random.randint(0, 16)), random.randint(0, 16)) for _ in range(4)])


def generate_random_objective():
    """
        Fonction permettant de générer un objectif aléatoirement.
        Un objectif est constitué d'une couleur et d'une forme.

        Retour
        ------
        String résultant de la concaténation d'une couleur et d'une forme.
    """
    color = random.choice(COLORS)
    shape = random.choice(SHAPES)
    return color + shape


def random_initialisation():
    """
        Fonction générant le plateau aléatoirement et l'affichant sur la console.

        Retours
        -------
        - une matrice représentant le plateau de jeu.
        - un tuple contenant des tuples (coordonnées des robots).
    """
    rand_quads = generate_random_board()
    board = combine_quads_into_single_board(
        rand_quads[0], rand_quads[1], rand_quads[2], rand_quads[3]
    )
    rand_positions = random_robots_positions()

    return board, rand_positions


def objective_initialisation(board):
    """
        Fonction permettant de générer aléatoirement un objectif (composé d'une couleur et d'une forme),
        et détermine les coordonnées de cet objectif.

        Paramètre
        ---------
        board: matrice représentant le plateau de jeu.

        Retour
        ------
        Tuple contenant les coordonnées de l'objectif, dans l'ordre (ligne, colonne).
    """
    obj = generate_random_objective()

    for i, line in enumerate(board.tolist()):
        for j, sq in enumerate(line):
            if obj in sq:
                return (i, j), obj


def printable_board(board, robot_positions):
    """
       Fonction retournant une autre version (plus aboutie visuellement) du plateau.
    """
    out = np.full((board.shape[0], board.shape[1], 4, 4), " ", dtype=np.str_)
    for y in range(board.shape[0]):
        for x in range(board.shape[1]):
            chars = "".join((c for c in board[y, x] if c not in DIRECTIONS + ("X",)))
            if len(chars) > 0:
                out[y, x, 1, 1] = chars[0]
            if len(chars) > 1:
                out[y, x, 1, 2] = chars[1]
            if (y, x) in robot_positions:
                out[y, x, 2, 1] = COLORS[robot_positions.index((y, x))]
            out[y, x, 0, 1:3].fill("#" if "N" in board[y, x] else "-")
            out[y, x, 1:3, 0].fill("#" if "W" in board[y, x] else "|")
            out[y, x, 3, 1:3].fill("#" if "S" in board[y, x] else "-")
            out[y, x, 1:3, 3].fill("#" if "E" in board[y, x] else "|")

    result = ""
    for y in range(board.shape[0]):
        for i in range(4 if y == board.shape[0] - 1 else 3):
            for x in range(board.shape[1]):
                for j in range(4 if x == board.shape[1] - 1 else 3):
                    result += out[y, x, i, j]
            result += "\n"
    return result


def show_board(board, robot_positions, objective, obj_coord):
    """
        Fonction permettant d'afficher le plateau, ainsi que l'objectif à atteindre.

        Paramètres
        ----------
        - board : matrice contenant le plateau de jeu.
        - robot_positions : tuples contenant les positions des robots.
        - objective : string représentant l'objectif.
        - obj_coord : tuple correspondant aux coordonnées de l'objectif 
    """
    print(printable_board(board, robot_positions))
    print(
        objective,
        "est l'objectif à atteindre, et il est situé à la ligne",
        obj_coord[0],
        "et à la colonne",
        obj_coord[1],
    )
