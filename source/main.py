#!/usr/bin/python3

import board_config as bc
from random import randint
import solver


def game():
    print("Bienvenue dans le jeu Ricochet Robot !")

    print(
        "\n----------------------------------------------------------------------------------------\n"
    )

    board, robot_positions = bc.random_initialisation()
    goal_coord, goal_name = bc.objective_initialisation(board)
    bc.show_board(board, robot_positions, goal_name, goal_coord)

    run_solver(board, robot_positions, goal_coord)


def run_solver(board, robot_positions, goal_positions):
    """
        Fonction permettant de créer le solveur 
        servant à déterminer le chemin optimal entre un robot et un objectif.
    """
    solv = solver.Solver(board)
    solution = solv.breadth_first_search(goal_positions, robot_positions)
    solv.show_solution(solution)


if __name__ == "__main__":
    game()
